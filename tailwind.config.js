module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    "./node_modules/flowbite/**/*.js"
  ],
  theme: {
    extend: {
      colors: {
        'usach-orange': {DEFAULT: "#EA7600"},
        'usach-blue': {DEFAULT: "#002F6C"},
        'usach-gray': {DEFAULT: "#B1B1B1"},
      }
    },
  },
  plugins: [
    require('flowbite/plugin')
  ],
}
import { createRouter, createWebHistory } from 'vue-router'

// Import views
import Home from '/src/components/Home.vue'
import About from '/src/components/About.vue'
import Diploma from '/src/components/Diploma.vue'
import Projects from '/src/components/Projects.vue'
import Sacvlc from '/src/components/SACVLC.vue'
import Laboratory from '/src/components/Laboratorio.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/diplomado-en-5g-tecnologia-politicas-publicas-y-modelos-de-negocio',
    name: 'Diploma',
    component: Diploma
  },
  {
    path: '/projects',
    name: 'Projects',
    component: Projects
  },
  {
    path: '/SACVLC',
    name: 'SACVLC',
    component: Sacvlc
  },  
  {
    path: '/mine-ventilation-lab',
    name: 'Lab',
    component: Laboratory
  },

]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router